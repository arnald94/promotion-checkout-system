# Kin+Carta Checkout Assignment #

This is a documentation of a promotion checkout system for Kin+Carta assignment

### Methodology ###

* I follow the Strategy Design Pattern to implement the promotions. 
In the future somebody can add other promotion strategies.


### Technologies / Tools ###

* Java 1.8
* Gradle 7.2
* Intellij 2021.3.3


### How to test ###

* Through terminal type "gradle test"
* Or through intellij run tests

### Improvements ###

* Add tests for methods (i dod not need to spend more time than the suggested)
* Save items to database