package com.checkout.engine;


import com.checkout.engine.domain.Item;
import com.checkout.engine.promotion.Promotion;
import com.checkout.engine.promotion.TenPercentDiscount;
import com.checkout.engine.promotion.WaterBottleDiscount;
import com.checkout.engine.repository.ItemRepo;
import com.checkout.engine.repository.ItemRepoImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BasketCheckoutTest {

    private List<Promotion> promotions;
    private ItemRepo itemRepo;

    @BeforeEach
    public void init() {
        itemRepo = new ItemRepoImpl();
        itemRepo.add(new Item("001", "Water Bottle", BigDecimal.valueOf(24.95)));
        itemRepo.add(new Item("002", "Hoodie", BigDecimal.valueOf(65.00)));
        itemRepo.add(new Item("003", "Sticker Set", BigDecimal.valueOf(3.99)));

        promotions = new ArrayList<Promotion>();
        promotions.add(new WaterBottleDiscount("001"));
        promotions.add(new TenPercentDiscount());
    }

    @Test
    public void FirstTestCase() {
        Checkout checkout = new CheckoutImpl(itemRepo, promotions);
        checkout.scan("001");
        checkout.scan("001");
        checkout.scan("002");
        checkout.scan("003");

        BigDecimal expectedCost = BigDecimal.valueOf(103.47);
        BigDecimal actualCost = checkout.totalAmount();

        assertEquals(expectedCost, actualCost);
    }

    @Test
    public void SecondTestCase() {
        Checkout checkout = new CheckoutImpl(itemRepo, promotions);
        checkout.scan("001");
        checkout.scan("001");
        checkout.scan("001");

        BigDecimal expectedCost = BigDecimal.valueOf(68.97);
        BigDecimal actualCost = checkout.totalAmount();

        assertEquals(expectedCost, actualCost);
    }

    @Test
    public void ThirdTestCase() {
        Checkout checkout = new CheckoutImpl(itemRepo, promotions);
        checkout.scan("002");
        checkout.scan("002");
        checkout.scan("003");

        BigDecimal expectedCost = BigDecimal.valueOf(120.59);
        BigDecimal actualCost = checkout.totalAmount();

        assertEquals(expectedCost, actualCost);
    }

}
