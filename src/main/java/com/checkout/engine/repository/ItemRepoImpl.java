package com.checkout.engine.repository;

import com.checkout.engine.domain.Item;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class ItemRepoImpl implements ItemRepo {

    Map<String, Item> items;

    public ItemRepoImpl() {
        items = new HashMap<>();
    }

    @Override
    public void add(Item item) {
        items.put(item.getId(), item);
    }

    @Override
    public Boolean exists(String id) {
        return items.get(id) != null;
    }

    @Override
    public String getName(String id) {
        if (items.get(id) == null) {
            throw new IllegalArgumentException();
        }
        return items.get(id).getName();
    }

    @Override
    public BigDecimal getPrice(String id) {
        if (items.get(id) == null) {
            throw new IllegalArgumentException();
        }
        return items.get(id).getPrice();
    }
}
