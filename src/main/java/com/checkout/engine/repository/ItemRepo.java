package com.checkout.engine.repository;

import com.checkout.engine.domain.Item;

import java.math.BigDecimal;

public interface ItemRepo {

    void add(Item item);

    Boolean exists(String id);

    String getName(String id);

    BigDecimal getPrice(String id);
}
