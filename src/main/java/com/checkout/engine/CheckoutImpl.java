package com.checkout.engine;

import com.checkout.engine.promotion.Promotion;
import com.checkout.engine.repository.ItemRepo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CheckoutImpl implements Checkout {

    private final static Logger logger = Logger.getLogger(CheckoutImpl.class.getName());

    private final ItemRepo itemRepo;
    private final List<Promotion> promotions;
    private final List<String> ids;

    public CheckoutImpl(com.checkout.engine.repository.ItemRepo itemRepo, List<Promotion> promotions) {
        this.itemRepo = itemRepo;
        this.promotions = promotions;
        ids = new ArrayList<String>();
    }

    @Override
    public void scan(String id) {
        logger.log(Level.INFO, "Item --> {0} is scanning!", id);

        if (!itemRepo.exists(id)) {
            throw new IllegalStateException();
        }
            ids.add(id);
    }

    @Override
    public BigDecimal totalAmount() {

        BasketImpl basketImpl = new BasketImpl(itemRepo, ids);

        logger.log(Level.INFO, "Initial Basket --> {0}", basketImpl);

        for (Promotion promotion : promotions){
            basketImpl = promotion.apply(basketImpl);
        }
        logger.log(Level.INFO, "Discounted Basket --> {0}", basketImpl);

        BigDecimal totalCost = basketImpl.getTotalCost();
        logger.log(Level.INFO, "Total cost of the basket --> {0}", totalCost);

        return totalCost;
    }
}
