package com.checkout.engine;

import java.math.BigDecimal;

public interface Basket {

    Long getTotalQuantity(String id);

    BigDecimal getFinalItemPrice(String id);

    void setDiscountedItemPrice(String id, BigDecimal discountPrice);

    BigDecimal getTotalPrice();

    Long getFreeQuantity(String id);

    void setFreeQuantity(String id, Long free);

    void increaseFreeQuantity(String id, Long free);

    void setTotalPrice(BigDecimal disountedPrice);

    BigDecimal getTotalCost();
}
