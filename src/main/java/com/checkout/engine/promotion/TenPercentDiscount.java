package com.checkout.engine.promotion;

import com.checkout.engine.BasketGroups;
import com.checkout.engine.BasketImpl;

import java.math.BigDecimal;
import java.util.Map;

public class TenPercentDiscount implements Promotion{

    private static final BigDecimal MIN_PRICE_OF_BASKET = BigDecimal.valueOf(75.00);
    private static final BigDecimal DISCOUNT = BigDecimal.valueOf(0.9);

    @Override
    public BasketImpl apply(BasketImpl basket) {
        if (basket.getTotalPrice().compareTo(MIN_PRICE_OF_BASKET) == 1){
            BigDecimal disountedPrice = basket.getTotalPrice().multiply(DISCOUNT);
            basket.setTotalPrice(disountedPrice);
        }
        return basket;
    }
}
