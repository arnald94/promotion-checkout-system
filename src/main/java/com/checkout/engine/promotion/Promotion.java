package com.checkout.engine.promotion;

import com.checkout.engine.BasketImpl;

public interface Promotion {

    BasketImpl apply(BasketImpl basket);
}
