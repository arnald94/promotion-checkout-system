package com.checkout.engine.promotion;

import com.checkout.engine.BasketImpl;

import java.math.BigDecimal;

public class WaterBottleDiscount implements Promotion {

    private static final int MIN_WATER_BOTTLE_FOR_DISCOUNT = 2;
    private static final BigDecimal WATER_BOTTLE_DISCOUNTED_PRICE = BigDecimal.valueOf(22.99);

    private final String id;

    public WaterBottleDiscount(String id) {
        this.id = id;
    }

    @Override
    public BasketImpl apply(BasketImpl basket) {
        Long boughtItemQuantity = basket.getTotalQuantity(id);

        if (boughtItemQuantity >= MIN_WATER_BOTTLE_FOR_DISCOUNT){
            basket.setDiscountedItemPrice(id, WATER_BOTTLE_DISCOUNTED_PRICE);
        }
        return basket;
    }
}
