package com.checkout.engine;

import com.checkout.engine.repository.ItemRepo;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BasketImpl implements Basket {

    ItemRepo itemRepo;
    Map<String, BasketGroups> basket;
    private BigDecimal totalCost;

    public BasketImpl(ItemRepo itemRepo, List<String> ids) {
        this.itemRepo = itemRepo;
        if (ids == null || itemRepo == null) {
            throw new IllegalArgumentException();
        }

        Map<String, Long> boughtItemsCount = ids.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        basket = boughtItemsCount.entrySet().stream().collect(HashMap<String, BasketGroups>::new, (map, item) -> {
            String sku = item.getKey();
            Long quantity = item.getValue();
            map.put(sku, initRow(sku, quantity));
        }, HashMap<String, BasketGroups>::putAll);
    }

    private BasketGroups initRow(String id, Long quantity) {
        BasketGroups group = new BasketGroups(quantity, 0L, itemRepo.getPrice(id), itemRepo.getPrice(id));
        return group;
    }

    @Override
    public Long getTotalQuantity(String id) {
        return basket.get(id) != null ? basket.get(id).getTotalQuantity() : 0;
    }

    @Override
    public BigDecimal getFinalItemPrice(String id) {
        return basket.get(id) != null ? basket.get(id).getFinalItemPrice() : BigDecimal.ZERO;
    }

    @Override
    public void setDiscountedItemPrice(String id, BigDecimal discountPrice) {
        if (basket.get(id) == null) {
            throw new IllegalArgumentException();
        }
        basket.get(id).setFinalItemPrice(discountPrice);
    }

    @Override
    public BigDecimal getTotalPrice() {
        this.totalCost =  basket.values().stream()
                .map(b -> b.getFinalItemPrice().multiply(BigDecimal.valueOf(b.getTotalQuantity() - b.getFreeQuantity())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return totalCost;
    }

    @Override
    public Long getFreeQuantity(String id) {
        return basket.get(id) != null ? basket.get(id).getFreeQuantity() : 0;
    }

    @Override
    public void setFreeQuantity(String id, Long free) {
        if (basket.get(id) == null) {
            throw new IllegalArgumentException();
        }
        basket.get(id).setFreeQuantity(free);
    }

    @Override
    public void increaseFreeQuantity(String id, Long free) {
        if (basket.get(id) == null) {
            throw new IllegalArgumentException();
        }
        Long presentQuantity = getFreeQuantity(id);
        basket.get(id).setFreeQuantity(presentQuantity + free);
    }

    @Override
    public void setTotalPrice(BigDecimal disountedPrice) {
        this.totalCost = disountedPrice;
    }

    @Override
    public BigDecimal getTotalCost(){
        return this.totalCost.setScale(2, BigDecimal.ROUND_DOWN);
    }

    @Override
    public String toString() {
        return this.basket.toString();
    }
}
