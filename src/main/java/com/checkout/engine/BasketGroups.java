package com.checkout.engine;

import java.math.BigDecimal;

public class BasketGroups {

    private long totalQuantity;
    private long freeQuantity;
    private BigDecimal initialItemPrice;
    private BigDecimal finalItemPrice;

    public BasketGroups(long totalQuantity, long freeQuantity, BigDecimal initialItemPrice, BigDecimal finalItemPrice) {
        this.totalQuantity = totalQuantity;
        this.freeQuantity = freeQuantity;
        this.initialItemPrice = initialItemPrice;
        this.finalItemPrice = finalItemPrice;
    }

    public long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public long getFreeQuantity() {
        return freeQuantity;
    }

    public void setFreeQuantity(long freeQuantity) {
        this.freeQuantity = freeQuantity;
    }

    public BigDecimal getInitialItemPrice() {
        return initialItemPrice;
    }

    public void setInitialItemPrice(BigDecimal initialItemPrice) {
        this.initialItemPrice = initialItemPrice;
    }

    public BigDecimal getFinalItemPrice() {
        return finalItemPrice;
    }

    public void setFinalItemPrice(BigDecimal finalItemPrice) {
        this.finalItemPrice = finalItemPrice;
    }

    @Override
    public String toString() {
        return "BasketGroups{" +
                "totalQuantity=" + totalQuantity +
                ", freeQuantity=" + freeQuantity +
                ", initialItemPrice=" + initialItemPrice +
                ", finalItemPrice=" + finalItemPrice +
                '}';
    }
}
