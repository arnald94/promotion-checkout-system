package com.checkout.engine;

import java.math.BigDecimal;

public interface Checkout {

    void scan(String id);

    BigDecimal totalAmount();
}
